/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulajava;

import java.util.Scanner;

/**
 *
 * @author Aluno
 */
public class Atividade15 {
     public static void main(String args[]) {
         float area, p, d, b, alt, valor1, valor2;
        Scanner input = new Scanner(System.in);
        
        System.out.println("Escreva a base: ");
        valor1 = input.nextFloat();
        System.out.println("Escreva a altura ");
        valor2 = input.nextFloat();
        System.out.println("Escreva o lado 1 ");
        float lad1 = input.nextFloat();
        System.out.println("Escreva o lado 2 ");
        float lad2 = input.nextFloat();
        System.out.println("Escreva o lado 3 ");
        float lad3 = input.nextFloat();
        
        area = (valor1 * valor2) / 2;
        p = lad1+ lad2 + lad3;
        d = (float) (Math.pow(lad1, 2) + Math.pow(lad2, 2));
        
         System.out.println("Area = " + area);
         System.out.println("Perimetro = " + p);
         System.out.println("Diagonal = "+ d);
         System.out.println("Altura = "+ valor2);
         System.out.println("Base = "+ valor1);
     }
}
